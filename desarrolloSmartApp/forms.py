from django import forms
from .models import *


class formLoginUsuario(forms.Form):
    usuario = forms.CharField(max_length=100)


class formLoginContrasena(forms.Form):
    contraseña = forms.CharField(max_length=100, widget=forms.PasswordInput)


class formNuevaCompraPresupuesto(forms.Form):
    presupuesto = forms.FloatField()


class formNuevaCompraIngresada(forms.Form):
    producto = forms.CharField(max_length=255)

class formNuevaCompraCantidad(forms.Form):
    cantidad = forms.IntegerField()

class formRegistroUsuario(forms.Form):
    nombre = forms.CharField(max_length=255)
    barrio = forms.CharField(max_length=255)
    direccion = forms.CharField(max_length=255)
    constrasena = forms.CharField(max_length=255)