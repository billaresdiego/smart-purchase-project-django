from django.contrib import admin
from .models import *

admin.site.register(Usuario)
admin.site.register(Compra)
admin.site.register(Categoria)
admin.site.register(SubCategoria)
admin.site.register(Supermercado)
admin.site.register(Producto_Supermercado)
admin.site.register(Item)
admin.site.register(Producto)
admin.site.register(Tarjeta)
