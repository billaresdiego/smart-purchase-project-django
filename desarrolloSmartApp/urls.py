from django.urls import path
from . import views


urlpatterns = [
    path(r'desarrolloSmartApp/Registro.html', views.paginaRegistro),
    path(r'desarrolloSmartApp/registro', views.registro),
    path(r'desarrolloSmartApp/Principal.html', views.login),
    path(r'desarrolloSmartApp/', views.principal),
    path(r'desarrolloSmartApp/index.html', views.paginaLogin),
    path(r'desarrolloSmartApp/NuevaCompra.html',views.nuevaCompra),
    path(r'desarrolloSmartApp/TerminarCompra.html',views.terminarCompra),
    path(r'desarrolloSmartApp/TarjDebitoCredito.html',views.tarjeta),
    path(r'desarrolloSmartApp/comprasHistorico.html',views.comprasHistorico),
    path(r'tarjeta/',views.tarjDebitoCredito)
]
