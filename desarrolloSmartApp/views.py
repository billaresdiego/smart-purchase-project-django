from django.shortcuts import render, redirect
from .models import *
from django.http import HttpResponse
from .forms import *

from django.forms import inlineformset_factory
from django.contrib.auth.forms import UserCreationForm

from django.forms import ModelForm 
from django.contrib.auth.models import User
from django import forms
from django.contrib import messages

nombre = ""
compras = 0
idProducto = []
cantidadCompra = []
precioUnitario = []
excesoDeCompra = ""

def paginaLogin(request):
        return render (request, 'recursosHTML/index.html')

def login(request):
                global nombre

                email = ""
                contrasena = ""
                redireccionarEmail = False
                redireccionarContrasena = False
                
                if request.POST["Email"]:
                        email = request.POST["Email"]
                        if not(Usuario.objects.filter(email=email).count()):
                                errorEmail = "El usuario ingresado es incorrecto"
                                return render(request, 'recursosHTML/index.html', 
                                                { "errorEmail":errorEmail })
                        else:
                                redireccionarEmail = True
                if request.POST["Contraseña"]:
                        contrasena = request.POST["Contraseña"]
                        if not(Usuario.objects.filter(email = email, 
                                                        contrasena=contrasena).count()):
                                 errorContrasena = "La contarseña ingresada es incorrecta"
                                 return render(request, 'recursosHTML/index.html', 
                                                { "errorContrasena":errorContrasena })
                        else:
                                redireccionarContrasena = True

                if redireccionarEmail and redireccionarContrasena:
                        nombre = email
                        usuario = Usuario.objects.filter(email=nombre)
                        nombrePersonaLogueada = usuario[0].nombre
                        apellidoPersonaLogueada = usuario[0].apellido

                        context = {
                                        "nombrePersonaLogueada":nombrePersonaLogueada,
                                        "apellidoPersonaLogueada":apellidoPersonaLogueada
                        }    
                        return render (request, 'recursosHTML/Principal.html', context)

                errorEmail = "Ingrese su usuario"
                errorContrasena = "Ingrese su contraseña"
                return render(request, 'recursosHTML/index.html', 
                                {  "errorEmail":errorEmail,
                                "errorContrasena":errorContrasena })
        
        
       
         
def principal(request):
        global nombre 
        usuario = Usuario.objects.filter(email=nombre)
        nombrePersonaLogueada = usuario[0].nombre
        apellidoPersonaLogueada = usuario[0].apellido

        context = {
                                "nombrePersonaLogueada":nombrePersonaLogueada,
                                "apellidoPersonaLogueada":apellidoPersonaLogueada
                  }    
        return render (request, 'recursosHTML/Principal.html', context)


def nuevaCompra(request):
        nuevaCompraPresupuesto = formNuevaCompraPresupuesto(request.POST or None)
        nuevaCompraIngresada = formNuevaCompraIngresada(request.POST or None)
        nuevaCompraCantidad = formNuevaCompraCantidad(request.POST or None)
        productos = Producto.objects.all()
        

        if nuevaCompraIngresada.is_valid() and nuevaCompraCantidad.is_valid():
                nuevaCompraCantidad_data = nuevaCompraCantidad.cleaned_data
                nuevaCompraIngresada_data = nuevaCompraIngresada.cleaned_data
                producto = nuevaCompraIngresada_data.get("producto")
                cantidad = nuevaCompraCantidad_data.get("cantidad")
                if not(Producto.objects.filter(id = producto).count()):
                        nuevaCompraIngresada.add_error("producto",'Este producto no esta en la lista')
                        

        if nuevaCompraPresupuesto.is_valid():
                nuevaCompraPresupuesto_data = nuevaCompraPresupuesto.cleaned_data
                presupuesto = nuevaCompraPresupuesto_data.get("presupuesto")
                global compras 
                global idProducto
                global cantidadCompra
                global precioUnitario
                global excesoDeCompra
                
                if Producto.objects.filter(id = producto).count() and presupuesto < (compras + (Producto.objects.get(id = producto).precio * cantidad)):
                        excesoDeCompra = 'El monto total de la compra excede el presupuesto ingresado'
                        nuevaCompraPresupuesto.add_error("presupuesto", 'El monto total de la compra excede el presupuesto ingresado')
                else:
                        if Producto.objects.filter(id = producto).count():
                                compras = compras + (Producto.objects.get(id = producto).precio * cantidad)
                                idProducto.append(producto)
                                cantidadCompra.append(cantidad)
                                precioUnitario.append(Producto.objects.get(id = producto).precio)
                                nuevaCompraPresupuesto.add_error("presupuesto", 'Monto actual de la compra: $' + str(compras))
        global nombre 
        usuario = Usuario.objects.filter(email=nombre)
        nombrePersonaLogueada = usuario[0].nombre
        apellidoPersonaLogueada = usuario[0].apellido     
        
        context = {
                "formNuevaCompraPresupuesto":nuevaCompraPresupuesto,
                "formNuevaCompraIngresada":nuevaCompraIngresada,
                "formNuevaCompraCantidad":nuevaCompraCantidad,
                'productos':productos,
                "nombrePersonaLogueada":nombrePersonaLogueada,
                "apellidoPersonaLogueada":apellidoPersonaLogueada
        }
        
        return render (request, 'recursosHTML/NuevaCompra.html',context)

def paginaRegistro(request):
        return render (request, 'recursosHTML/Registro.html')

def registro(request):
        if request.POST['usuario']:
                usuario = request.POST['usuario']

        if request.POST['apellido']:
                apellido = request.POST['apellido']

        if request.POST['seleccionSexo']:
                seleccionSexo = str(request.POST['seleccionSexo'])

        if request.POST['nacimiento']:
                nacimiento = request.POST['nacimiento']
        
        if request.POST['barrio']:
                barrio = request.POST['barrio']

        if request.POST['direccion']:
                direccion = request.POST['direccion']

        if request.POST['email']:
                controlEmail = request.POST['email']

                if Usuario.objects.filter(email = controlEmail).count():
                        errorEmail = "El email ingresado ya existe"
                        return render(request, 'recursosHTML/Registro.html', {'errorEmail':errorEmail})
                if '@' in controlEmail:
                        email = request.POST['email']
                else:
                        errorEmail = "El email ingresado debe poseer el caracter @"
                        return render(request, 'recursosHTML/Registro.html', {'errorEmail':errorEmail})
               

        if request.POST['contrasena1'] and request.POST['contrasena2']:
                controlContrasena = request.POST['contrasena1']

                if len(controlContrasena) < 6:
                        errorContrasenas = "Su contraseña debe ser mayor de 6 caracteres"
                        return render(request, 'recursosHTML/Registro.html', {'errorContrasenas':errorContrasenas})

                if not(request.POST['contrasena1'] == request.POST['contrasena2']):
                        errorContrasenas = "Contraseñas inválidas"
                        return render(request, 'recursosHTML/Registro.html', {'errorContrasenas':errorContrasenas})

                contrasena = request.POST['contrasena1']
                
        
        
        Usuario.objects.create(nombre=usuario, apellido=apellido, seleccionSexo=seleccionSexo,
                        nacimiento=nacimiento, barrio=barrio, direccion=direccion, 
                        email=email, contrasena=contrasena)
        
        
        return render(request, 'recursosHTML/index.html')
        
def terminarCompra(request):
        global nombre
        global compras
        global idProducto
        global cantidadCompra
        global precioUnitario
        global excesoDeCompra 

        usuario = Usuario.objects.filter(email=nombre)
        nombrePersonaLogueada = usuario[0].nombre
        apellidoPersonaLogueada = usuario[0].apellido

        if not(compras == 0): 
                Compra.objects.create(idUsuario = Usuario.objects.filter(email = nombre)[0], monto_compra = compras)

                idCompraUltimo = Compra.objects.latest('id')
                for idx, val in enumerate(idProducto):
                        Item.objects.create(idCompra = idCompraUltimo, idProducto = Producto.objects.get(id = idProducto[idx]), cantidad = cantidadCompra[idx], precioUnitario = precioUnitario[idx])

        compras = 0
        precioUnitario = []
        idProducto = []
        cantidadCompra = []
        

        context = {
                        "nombrePersonaLogueada":nombrePersonaLogueada,
                        "apellidoPersonaLogueada":apellidoPersonaLogueada
                  }   
        return render(request, 'recursosHTML/TerminarCompra.html', context)

def tarjDebitoCredito(request):
        if request.POST['tarjeta']:
                tarjeta = str(request.POST['tarjeta'])

        if tarjeta=='Credito':
                if request.POST['tipo1']:
                        tipo1 = request.POST['tipo1']
                if request.POST['titular1']:
                        titular1 = request.POST['titular1']
                if request.POST['num1']:
                        num1 = request.POST['num1']
                if request.POST['cod1']:
                        cod1 = request.POST['cod1']
                if request.POST['vencimiento1']:
                        vencimiento1 = request.POST['vencimiento1']
                Tarjeta.objects.create(tipo=tipo1, titular=titular1, num=num1, cod=cod1, 
                                vencimiento=vencimiento1)
        
        if tarjeta=='Debito':
                if request.POST['tipo2']:
                        tipo2 = request.POST['tipo2']
                if request.POST['titular2']:
                        titular2 = request.POST['titular2']
                if request.POST['num2']:
                        num2 = request.POST['num2']
                if request.POST['cod2']:
                        cod2 = request.POST['cod2']
                if request.POST['vencimiento2']:
                        vencimiento2 = request.POST['vencimiento2']
                Tarjeta.objects.create(tipo=tipo2, titular=titular2, num=num2, cod=cod2, 
                                vencimiento=vencimiento2)

        global nombre 
        usuario = Usuario.objects.filter(email=nombre)
        nombrePersonaLogueada = usuario[0].nombre
        apellidoPersonaLogueada = usuario[0].apellido

        context = {
                        "nombrePersonaLogueada":nombrePersonaLogueada,
                        "apellidoPersonaLogueada":apellidoPersonaLogueada
                  }   

        return render(request, './', context)

def tarjeta(request):
        global nombre 
        usuario = Usuario.objects.filter(email=nombre)
        nombrePersonaLogueada = usuario[0].nombre
        apellidoPersonaLogueada = usuario[0].apellido

        context = {
                        "nombrePersonaLogueada":nombrePersonaLogueada,
                        "apellidoPersonaLogueada":apellidoPersonaLogueada
                  }   
        return render (request, 'recursosHTML/TarjDebitoCredito.html',context)

def comprasHistorico(request):
        global nombre
        productos = Producto.objects.all()
        compras = Compra.objects.filter(idUsuario = Usuario.objects.filter(email = nombre)[0])

        items = Item.objects.all()
        itemsComprados = []
        for compra in compras:
                for item in items:
                        if(compra == item.idCompra):
                                        itemsComprados.append(item) 
                                        print(item.idCompra)        
        
        usuario = Usuario.objects.filter(email=nombre)
        nombrePersonaLogueada = usuario[0].nombre
        apellidoPersonaLogueada = usuario[0].apellido 
                
        context = {
                'compras':compras,
                'productos':productos,
                "nombrePersonaLogueada":nombrePersonaLogueada,
                "apellidoPersonaLogueada":apellidoPersonaLogueada,
                "itemsComprados":itemsComprados
        }
        return render (request, 'recursosHTML/comprasHistorico.html',context)

       
