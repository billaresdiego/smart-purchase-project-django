from django.db import models


#TABLA USUARIO
class Usuario(models.Model):
    nombre=models.CharField(max_length= 255)
    apellido=models.CharField(max_length= 255)
    seleccionSexo=models.CharField(max_length= 255)
    nacimiento=models.DateField()
    barrio=models.CharField(max_length= 255)
    direccion=models.CharField(max_length= 255)
    email=models.EmailField(unique = True)
    contrasena=models.CharField(max_length= 255)

    def __str__(self):
        return self.nombre
    

#TABLA COMPRA
class Compra(models.Model):
    idUsuario=models.ForeignKey(Usuario, on_delete=models.CASCADE)
    fecha=models.DateTimeField(auto_now_add=True, auto_now = False)
    monto_compra=models.IntegerField()
   


#TABLA CATEGORIA
class Categoria(models.Model):
    nombreCategoria=models.CharField(max_length= 255)

#TABLA CATEGORIA
class SubCategoria(models.Model):
    idCategoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    nombreCategoria=models.CharField(max_length= 255)
    

#TABLA PRODUCTO
class Producto(models.Model):
    nombre=models.CharField(max_length= 255)
    precio=models.IntegerField()
    marca=models.CharField(max_length= 255)
    idSubCategoria = models.ForeignKey(SubCategoria, on_delete=models.CASCADE)
    imagen=models.ImageField(upload_to='static/diseñoSmart/imagenes/ImagenesCatalogo')
    

#TABLA Item
class Item(models.Model):
    idCompra=models.ForeignKey(Compra, on_delete=models.CASCADE)
    idProducto=models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad=models.IntegerField()
    precioUnitario=models.IntegerField()
    
    def __eq__(self, other): 
        if isinstance(other, Item): 
            return self.idCompra == other.idCompra
        return NotImplemented
    

#TABLA SUPERMERCADO
class Supermercado(models.Model):
    nombre=models.CharField(max_length= 255)
    direccion=models.CharField(max_length= 255)
    horario=models.CharField(max_length= 255)
    telefono=models.IntegerField()
    

#TABLA PRODUCTOS_SUPERMERCADO
class Producto_Supermercado(models.Model):
    idProducto=models.ForeignKey(Producto, on_delete=models.CASCADE)
    idSupermercado=models.ForeignKey(Supermercado, on_delete=models.CASCADE)

#TABLA TARJETA
class Tarjeta(models.Model):
    tipo=models.CharField(max_length= 255)
    titular=models.CharField(max_length= 255)
    num=models.IntegerField()
    cod=models.IntegerField()
    vencimiento=models.DateField()
    