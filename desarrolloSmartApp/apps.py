from django.apps import AppConfig


class DesarrollosmartappConfig(AppConfig):
    name = 'desarrolloSmartApp'
