# Generated by Django 3.0.4 on 2020-03-25 02:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombreCategoria', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Compra',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('monto_compra', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Producto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=255)),
                ('precio', models.IntegerField()),
                ('marca', models.CharField(max_length=255)),
                ('imagen', models.ImageField(upload_to='')),
            ],
        ),
        migrations.CreateModel(
            name='Supermercado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=255)),
                ('direccion', models.CharField(max_length=255)),
                ('horario', models.CharField(max_length=255)),
                ('telefono', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Tarjeta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', models.CharField(max_length=255)),
                ('titular', models.CharField(max_length=255)),
                ('num', models.IntegerField()),
                ('cod', models.IntegerField()),
                ('vencimiento', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=255)),
                ('apellido', models.CharField(max_length=255)),
                ('seleccionSexo', models.CharField(max_length=255)),
                ('nacimiento', models.DateField()),
                ('barrio', models.CharField(max_length=255)),
                ('direccion', models.CharField(max_length=255)),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('contrasena', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='SubCategoria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombreCategoria', models.CharField(max_length=255)),
                ('idCategoria', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='desarrolloSmartApp.Categoria')),
            ],
        ),
        migrations.CreateModel(
            name='Producto_Supermercado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('idProducto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='desarrolloSmartApp.Producto')),
                ('idSupermercado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='desarrolloSmartApp.Supermercado')),
            ],
        ),
        migrations.AddField(
            model_name='producto',
            name='idSubCategoria',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='desarrolloSmartApp.SubCategoria'),
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad', models.IntegerField()),
                ('precioUnitario', models.IntegerField()),
                ('idCompra', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='desarrolloSmartApp.Compra')),
                ('idProducto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='desarrolloSmartApp.Producto')),
            ],
        ),
        migrations.AddField(
            model_name='compra',
            name='idUsuario',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='desarrolloSmartApp.Usuario'),
        ),
    ]
